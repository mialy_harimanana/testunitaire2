package testUnitairePackage;

class Money {
	private int fAmount;
	private String fCurrency;
	
	public Money(int amount, String currency)
	{ fAmount = amount;
	fCurrency = currency;
	}
	
	public int amount() {
	return fAmount;
	}
	
	public String currency() {
	return fCurrency;
	}
	
	public Money add(Money m) {
	return new Money(amount() + m.amount(), currency());
	}
	
	public boolean equals(Object obj) {
		boolean response = false ;
		if(obj !=null) {
			if (obj.getClass().isAssignableFrom(this.getClass())) {
				Money m = (Money)obj;
				if (this.amount() == m.amount() && this.currency() == m.currency()) {
					response = true;
				}
			}
		}
		return response ;
	}
}